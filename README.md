# STAR WARS API

Projeto WEB API ASP NET CORE:


A API provê três endpoints:
              
	1 - GET /characters (Obtém todos os personagens e ordená-los por quantidade de filmes que aparecem, usando nome do personagem como segundo critério de ordenação (ordem alfabética))
	
	2 - GET /characters/{idPersonagem} (Obtém um personagem por ID e trazer nome do personagem, ano de nascimento e nome dos filmes que está);
	
	3 - GET /characters/humans (Obtém a lista de todos os personagens humanos e a média de peso dentre eles.)
