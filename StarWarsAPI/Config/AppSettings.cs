namespace StarWarsAPI.Config
{
    public class AppSettings
    {

        public string StarWarsAPiPeopleBaseUrl {get;set;}
        
        public string StarWarsAPiFilmsBaseUrl {get;set;}

        public string StarWarsAPiSpeciesBaseUrl {get;set;}
        
    }
}