
namespace StarWarsAPI.DTO
{

    public class Starship : Transport
    {

        public float hyperdrive_rating { get; set; }

        public string MGLT { get; set; }

        public string starship_class { get; set; }

    }
}