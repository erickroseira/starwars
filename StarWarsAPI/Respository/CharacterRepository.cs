using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StarWarsAPI.Config;
using StarWarsAPI.HTTPRequest;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Respository
{
    public class CharacterRepository : ICharacterRepository
    {

        private readonly IOptions<AppSettings> _appSettings;
        private readonly IWebRequest _webRequest;

        public CharacterRepository(IOptions<AppSettings> appSettings, IWebRequest webRequest)
        {

            _appSettings = appSettings;
            _webRequest = webRequest;
        }

        public async Task<IEnumerable<Person>> getAllAsync()
        {

            try
            {

                bool hasNext = true;
                var resourceUrl = _appSettings.Value.StarWarsAPiPeopleBaseUrl;
                var lPeople = new List<Person>();

                while (hasNext)
                {
                    var jsonString = await _webRequest.GetStringAsync(resourceUrl);

                    var jsonObject = JObject.Parse(jsonString);

                    var results = jsonObject["results"].Children().ToList();

                    results.ForEach(person => lPeople.Add(person.ToObject<Person>()));

                    hasNext = jsonObject["next"].Type != JTokenType.Null;

                    resourceUrl = jsonObject["next"]?.ToString();
                }

                return lPeople;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<Person> getCharacterAsync(string pUrlCharacter)
        {
            try
            {
                var characterJsonString = await _webRequest.GetStringAsync(pUrlCharacter);

                if (characterJsonString != null)
                {

                    var character = JsonConvert.DeserializeObject<Person>(characterJsonString);

                    return character;

                }

                return default(Person);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Person> getCharacterByIdAsync(int pId)
        {
            try
            {

                var resourceUrl = _appSettings.Value.StarWarsAPiPeopleBaseUrl;

                var jsonString = await _webRequest.GetStringAsync(resourceUrl + pId);

                if (jsonString != null)
                {

                    var person = JsonConvert.DeserializeObject<Person>(jsonString);

                    return person;
                }

                return default(Person);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}