using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Respository
{
    public interface IFilmRepository
    {
         Task<Film> getFilmAsync(string pUrlFilm);
    }
}