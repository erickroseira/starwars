using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using StarWarsAPI.Config;
using StarWarsAPI.DTO;
using StarWarsAPI.HTTPRequest;

namespace StarWarsAPI.Respository
{
    public class SpecieRepository : ISpecieRepository
    {

        private readonly IOptions<AppSettings> _appSettings;
        private readonly IWebRequest _webRequest;


        public SpecieRepository(IOptions<AppSettings> appSettings, IWebRequest webRequest)
        {
            _appSettings = appSettings;
            _webRequest = webRequest;
        }

        public async Task<IEnumerable<Specie>> getAllAsync()
        {
             try
            {

                bool hasNext = true;
                var resourceUrl = _appSettings.Value.StarWarsAPiSpeciesBaseUrl;
                var lSpecies = new List<Specie>();

                while (hasNext)
                {
                    var jsonString = await _webRequest.GetStringAsync(resourceUrl);

                    var jsonObject = JObject.Parse(jsonString);

                    var results = jsonObject["results"].Children().ToList();

                    results.ForEach(specie => lSpecies.Add(specie.ToObject<Specie>()));

                    hasNext = jsonObject["next"].Type != JTokenType.Null;

                    resourceUrl = jsonObject["next"]?.ToString();
                }

                return lSpecies;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}