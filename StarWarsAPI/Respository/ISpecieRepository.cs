using System.Collections.Generic;
using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Respository
{
    public interface ISpecieRepository
    {
         Task<IEnumerable<Specie>> getAllAsync();
    }
}