using System.Collections.Generic;
using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Respository
{
    public interface ICharacterRepository
    {
         Task<IEnumerable<Person>> getAllAsync();

         Task<Person> getCharacterByIdAsync(int pId);

         Task<Person> getCharacterAsync(string pUrlFilm);

    }
}