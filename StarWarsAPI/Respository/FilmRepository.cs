using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StarWarsAPI.Config;
using StarWarsAPI.DTO;
using StarWarsAPI.HTTPRequest;

namespace StarWarsAPI.Respository
{
    public class FilmRepository : IFilmRepository
    {

        private readonly IWebRequest _webRequest;

        public FilmRepository(IWebRequest webRequest)
        {
            _webRequest = webRequest;
        }

        public async Task<Film> getFilmAsync(string pUrlFilm)
        {
            try
            {
                var filmJsonString = await _webRequest.GetStringAsync(pUrlFilm);

                if (filmJsonString != null)
                {

                    var film = JsonConvert.DeserializeObject<Film>(filmJsonString);

                    return film;
                }

                return default(Film);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}