using System.Threading.Tasks;
using StarWarsAPI.DTO;
using StarWarsAPI.Respository;

namespace StarWarsAPI.Service
{
    public class FilmService : IFilmService
    {

        private readonly IFilmRepository _filmRepository;

        public FilmService(IFilmRepository filmRepository)
        {
            _filmRepository = filmRepository;
        }

        public Task<Film> GetFilm(string pUrlFilm)
        {
            var Film = _filmRepository.getFilmAsync(pUrlFilm);

            return Film;
        }
    }
}