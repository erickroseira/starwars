using System.Collections.Generic;
using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Service
{
    public interface ISpecieService
    {
         Task<IEnumerable<Specie>> getAll();
    }
}