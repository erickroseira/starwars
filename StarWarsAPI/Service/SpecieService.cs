using System.Collections.Generic;
using System.Threading.Tasks;
using StarWarsAPI.DTO;
using StarWarsAPI.Respository;

namespace StarWarsAPI.Service
{
    public class SpecieService : ISpecieService
    {

        private readonly ISpecieRepository _specieRepository;

        public SpecieService(ISpecieRepository specieRepository)
        {
            _specieRepository = specieRepository;
        }

        public async Task<IEnumerable<Specie>> getAll()
        {
            IEnumerable<Specie> lSpecies = await _specieRepository.getAllAsync();

            return lSpecies;
        }
    }
}