using System.Collections.Generic;
using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Service
{

    public interface ICharacterService
    {

        Task<IEnumerable<Person>> GetAllCharactersOrderedByCountByName();

        Task<Person> GetCharacterById(int pId);

        Task<Person> GetCharacter(string pUrlCharacter);

        double ComputeWeightAverage(IEnumerable<Person> pCharacters);

    }
}