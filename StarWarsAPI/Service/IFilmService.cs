using System.Threading.Tasks;
using StarWarsAPI.DTO;

namespace StarWarsAPI.Service
{
    public interface IFilmService
    {
         Task<Film> GetFilm(string pUrlFilm);
    }
}