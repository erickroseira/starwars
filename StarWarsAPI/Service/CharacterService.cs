using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StarWarsAPI.DTO;
using StarWarsAPI.Respository;


namespace StarWarsAPI.Service
{

    public class CharacterService : ICharacterService
    {
        private readonly ICharacterRepository _characterRepository;

        public CharacterService(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public async Task<IEnumerable<Person>> GetAllCharactersOrderedByCountByName()
        {
            IEnumerable<Person> lPeople = await _characterRepository.getAllAsync();

            return lPeople.ToList().OrderByDescending(x => x.films.Count()).ThenBy(y => y.name);
        }

        public async Task<Person> GetCharacterById(int pId)
        {
            Person person = await _characterRepository.getCharacterByIdAsync(pId);

            return person;
        }

        public async Task<Person> GetCharacter(string pUlrCharacter)
        {
            Person person = await _characterRepository.getCharacterAsync(pUlrCharacter);

            return person;
        }

        public double ComputeWeightAverage(IEnumerable<Person> pCharacters)
        {
            try
            {
                double totalWeight = 0, weightAvarage;

                foreach (var character in pCharacters)
                {
                    if(!int.TryParse(character.mass, out int number))
                        continue;

                    totalWeight += Convert.ToDouble(character.mass);
                }

                weightAvarage = totalWeight / pCharacters.Where(x => int.TryParse(x.mass, out int number)).Count();                

                return weightAvarage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}