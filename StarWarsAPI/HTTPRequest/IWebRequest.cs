using System.Threading.Tasks;

namespace StarWarsAPI.HTTPRequest
{
    public interface IWebRequest
    {
         
         Task<string> GetStringAsync(string pResourceUrl);

    }
}