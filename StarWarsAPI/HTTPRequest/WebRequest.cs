using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace StarWarsAPI.HTTPRequest
{
    public class WebRequest : IWebRequest
    {

        public async Task<string> GetStringAsync(string pResourceUrl)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(pResourceUrl);

                    if (response.IsSuccessStatusCode)
                    {

                        var jsonString = await response.Content.ReadAsStringAsync();

                        return jsonString;
                    }

                    return null;
                }
            }
            catch (HttpRequestException ex)
            {
                throw ex;
            }
        }
    }
}