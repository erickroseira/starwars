using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StarWarsAPI.DTO;
using StarWarsAPI.Service;
using System.Linq;
using System.Net;

namespace StarWarsAPI.Controllers
{

    [Route("api/characters")]
    [ApiController]
    public class CharacterController : ControllerBase
    {

        private readonly ICharacterService _characterService;

        private readonly IFilmService _filmService;

        private readonly ISpecieService _specieService;


        public CharacterController(ICharacterService characterService, IFilmService filmService, ISpecieService specieService)
        {
            _characterService = characterService;
            _filmService = filmService;
            _specieService = specieService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCharactersOrderedByCountByNameAsync()
        {
            try
            {
                IEnumerable<Person> lPeople = await _characterService.GetAllCharactersOrderedByCountByName();
                return Ok(lPeople);
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetCharacterByIdAsync(int id)
        {
            try
            {
                Person lPerson = await _characterService.GetCharacterById(id);

                if (lPerson == null)
                    return NotFound(new { StatusCode = HttpStatusCode.NotFound, Message = "The Character was not found." });

                var lFilms = new List<Film>();

                foreach (var urlFilm in lPerson.films)
                {
                    var film = await _filmService.GetFilm(urlFilm);

                    lFilms.Add(film);
                }


                var response = new
                {
                    Name = lPerson.name,
                    BirthYear = lPerson.birth_year,
                    Films = lFilms.Select(x => x.title).ToList()
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet("humans")]
        public async Task<IActionResult> GetAllHumanCharacters()
        {
            try
            {

                IEnumerable<Specie> lSpecies = await _specieService.getAll();

                var humanSpecie = lSpecies.Where(x => x.name.Equals("Human")).FirstOrDefault();

                if (humanSpecie != null)
                {

                    var lCharacters = new List<Person>();

                    foreach (var urlCharacter in humanSpecie.people)
                    {
                        var character = await _characterService.GetCharacter(urlCharacter);

                        lCharacters.Add(character);
                    }

                    var weightAvarage = _characterService.ComputeWeightAverage(lCharacters);

                    var response = new
                    {
                        WeightAvarage = weightAvarage,
                        HumanCharacters = lCharacters
                    };

                    return Ok(response);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}